// toBe uses Object.is to test precise equality.
test('toBe matcher test', () => {
  expect(10 + 7).toBe(17);

});

// toEqual recursively checks every field of an object or array
test('toEqual matcher test', () => {
  const myNumbers = {one: 1};
  myNumbers['two'] = 2;
  expect(myNumbers).toEqual({one: 1, two:2});

});

// not allows you to test for the opposite of a matcher.
test('not matcher test', () => {
  for (let a = 1; a < 10; a++) {
    for (let b = 1; b < 10; b++) {
      expect(a + b).not.toBe(0);

    }
  }
});

// Truthiness matchers
// Used to distinguish between undefined, null, and false
test('truthy helpers test with null', () => {
  const nullVariable = null;
  expect(nullVariable).toBeNull();
  expect(nullVariable).toBeDefined();
  expect(nullVariable).not.toBeUndefined();
  expect(nullVariable).not.toBeTruthy();
  expect(nullVariable).toBeFalsy();

});

test('truthy helpers test with zero', () => {
  const zero = 0;
  expect(zero).not.toBeNull();
  expect(zero).toBeDefined();
  expect(zero).not.toBeUndefined();
  expect(zero).not.toBeTruthy();
  expect(zero).toBeFalsy();

});
