A demonstration of Jest's Matchers.

Jest is a JavaScript test runner that lets you access the DOM via jsdom.

# Setup:
[Install NPM](https://www.npmjs.com/get-npm)

## Install Modules:
`npm install`

# To run the tests:
`npm run test`
